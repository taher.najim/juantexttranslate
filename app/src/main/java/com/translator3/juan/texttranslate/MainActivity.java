package com.translator3.juan.texttranslate;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;


public class MainActivity extends AppCompatActivity {

    private DatabaseAccess databaseAccess;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        this.databaseAccess = DatabaseAccess.getInstance(getApplicationContext());
        final Spinner dropdown = findViewById(R.id.spinner2);
        Button button1 = findViewById(R.id.button1);
        final EditText editText = findViewById(R.id.editText);
        final EditText editText2 = findViewById(R.id.editText2);

        //Language to Translate to Drop down menu
        String[] items = new String[]{"Filipino", "Bikolano"};
        ArrayAdapter<String> adapter = new ArrayAdapter<>(this, android.R.layout.simple_spinner_dropdown_item, items);
        dropdown.setAdapter(adapter);

        button1.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                String languageSelected = dropdown.getSelectedItem().toString(); //get the language to translate to
                String inputWord = editText.getText().toString(); //get the input word

                databaseAccess.open();
                String translatedWord = databaseAccess.getTranslate(languageSelected, inputWord); //Get translated words
                databaseAccess.close();
                editText2.setText(translatedWord); //Output the translated word
            }
        });




    }
}
