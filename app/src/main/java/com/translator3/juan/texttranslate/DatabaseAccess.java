package com.translator3.juan.texttranslate;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.text.TextUtils;
import android.view.View;


public class DatabaseAccess {
    private SQLiteOpenHelper openHelper;
    private SQLiteDatabase database;
    private static DatabaseAccess instance;

    /**
     * Private constructor to aboid object creation from outside classes.
     *
     * @param context
     */
    private DatabaseAccess(Context context) {
        this.openHelper = new DatabaseOpenHelper(context);
    }

    /**
     * Return a singleton instance of DatabaseAccess.
     *
     * @param context the Context
     * @return the instance of DabaseAccess
     */
    public static DatabaseAccess getInstance(Context context) {
        if (instance == null) {
            instance = new DatabaseAccess(context);
        }
        return instance;
    }

    /**
     * Open the database connection.
     */
    public void open() {
        this.database = openHelper.getWritableDatabase();
    }

    /**
     * Close the database connection.
     */
    public void close() {
        if (database != null) {
            this.database.close();
        }
    }

    /**
     * Read language words from the database.
     *
     * @return a function word
     */
    public String getTranslate(String languageSelected, String inputWord) {
        String resultTranslate = "";

        try {
            Cursor cursor = database.rawQuery("SELECT "+languageSelected+" FROM languagetb WHERE English = '"+inputWord+"'", null);
            cursor.moveToFirst();
            String resultWord = cursor.getString(cursor.getColumnIndex(languageSelected));
            cursor.close();
            resultTranslate = resultWord;
        } catch (Exception e) {
            e.printStackTrace();
            String[] resultPhrase = inputWord.split(" "); //split the phrase to words
            String[] resultArray = new String[resultPhrase.length]; //create resultArray
            int i;
            for (i = 0; i < resultPhrase.length; i++) { //loop translate each word
                String inputWord1 = resultPhrase[i];
                try {
                    Cursor cursor1 = database.rawQuery("SELECT " + languageSelected + " FROM languagetb WHERE English = '" + inputWord1 + "'", null);
                    cursor1.moveToFirst();
                    resultArray[i] = cursor1.getString(cursor1.getColumnIndex(languageSelected));
                    cursor1.close();
                    resultTranslate = TextUtils.join(" ", resultArray); //join the string array to one string
                } catch (Exception ee){
                    ee.printStackTrace();
                    resultArray[i] = inputWord1;
                    resultTranslate = TextUtils.join(" ", resultArray); //join the string array to one string
                }
            }//end of for loop
        }//end of catch block

        return resultTranslate; //return the string of translated words
    }//end of getTranslate method
}